# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import os
from urllib.parse import urljoin
from urllib.parse import urlparse
import json

class ScrapcdmPipeline:
    def process_item(self, item, spider):
         url = item['url']
         path= item['path']
         os.system('wget -O "%s" "%s"' % (path, url))
         outdir = os.path.join("data")
         directory = item['directory']
         path = os.path.join(outdir, directory)
         records_file = os.path.join(path, 'records.json')
         records=item['records']
         with open(records_file, 'w') as filey:
            filey.write(json.dumps(records, indent=4))
         records.clear 
         return item
