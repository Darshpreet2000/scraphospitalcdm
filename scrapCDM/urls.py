urls = [
        ['https://www.atlanticare.org/patients-and-visitors/for-patients/billing-and-insurance/hospital-charge-list',"https://www.atlanticare.org/","AtlantiCare Regional Medical Center"],
          ['https://www.aurorahealthcare.org/patients-visitors/billing-payment/health-care-costs#Auroras-Standard-Charges',"https://www.aurorahealthcare.org/","Aurora Health Care"],
       ['https://www.huntsvillehospital.org/price-transparency',"https://www.huntsvillehospital.org/","Huntsville Hospital"],
        ['https://www.ucsfhealth.org/about/pricing-transparency','https://www.ucsfhealth.org/',"University of California San Francisco"],
       ['https://www.keckmedicine.org/hospital-pricing-information/','',"Keck Medicine of USC"],
        ['https://health.ucdavis.edu/newsroom/public-reporting/chargemaster.html','https://health.ucdavis.edu/',"University of California, Davis Medical Center"],
      ['https://health.ucsd.edu/patients/billing/standard-charges/Pages/default.aspx','https://health.ucsd.edu',"UC San Diego Health"]
    
        ]
