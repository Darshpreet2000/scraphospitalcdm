
import scrapy
import os
from urllib.parse import urljoin
from urllib.parse import urlparse
import json
import datetime
from scrapCDM.items import ScrapcdmItem
from bs4 import BeautifulSoup
from scrapCDM.urls import urls

today = datetime.datetime.today().strftime('%Y-%m-%d')

class ExampleSpider(scrapy.Spider):
    name = 'example'
    def start_requests(self):
        for url in urls:
            yield scrapy.Request(url=url[0], callback=self.parse,meta={'index':url[1],'name':url[2]})
            

    def parse(self, response):
     records = []     
     soup = BeautifulSoup(response.text, 'lxml')
     for entry in soup.find_all('a', href=True):
         link =  entry['href']
         entry_name = entry.text.strip()
         self.log(link)
         if '.csv' in link:
            base_url=response.meta['index']
            link = base_url + link 
            self.logger.info(link)
            yield scrapy.Request(link, callback=self.save_pdf,meta={'hospitalId':entry_name,'name':response.meta['name'],'records':records})
         elif '.xlsx' in link:
            base_url=response.meta['index']
            link = base_url + link
            self.logger.info(link)
            yield scrapy.Request(link, callback=self.save_pdf,meta={'hospitalId':entry_name,'name':response.meta['name'],'records':records})
              # Keep json record of all files included 
          
    def save_pdf(self, response):   
     filename =  os.path.basename(response.url.split('?')[0])    
     outdir = os.path.join("data")
     directory = response.meta['name']
     path = os.path.join(outdir, directory)
     if os.path.isdir(path)==False: 
      os.mkdir(path)  
     path = os.path.join(path, filename)
     self.log("\n\n\n We got data! \n\n\n"+ path)
     self.logger.info('Saving PDF %s', path)
     records=response.meta['records']
     record = { 
                   'filename': filename,
                   'hospitalId': response.meta['hospitalId'], #sub hospital name
                   'name':response.meta['name'] #hospital name in overpass api
                   }
     records.append(record)
     item=ScrapcdmItem()
     item['url']=response.url
     item['path']=path
     item['directory']=directory
     item['records']=records 
     yield item
     